using System;
using System.Collections;
using System.Diagnostics;

namespace PersistentStack
{
    /// <summary>
    /// Represents a proxy-type for nongeneric collection.
    /// </summary>
    internal class CollectionDebugView
    {
        private ICollection collection;

        /// <summary>
        /// Constructs the new instance of CollectionDebugView from the particular collection.
        /// </summary>
        /// <param name="collection">The collection.</param>
        public CollectionDebugView(ICollection collection)
        {
            this.collection = collection ?? throw new ArgumentNullException(nameof(collection));
        }

        /// <summary>
        /// The current collection items.
        /// </summary>
        [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
        public object[] Items
        {
            get
            {
                object[] items = new object[collection.Count];
                collection.CopyTo(items, 0);
                return items;
            }
        }
    }
}