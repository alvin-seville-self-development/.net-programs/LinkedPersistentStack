using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace PersistentStack
{
    /// <summary>
    /// Represents a strongly typed persistent stack.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    [DebuggerTypeProxy(typeof(CollectionDebugView))]
    [DebuggerDisplay("Count = {Count}")]
    [Serializable]
    public partial class PersistentStack<T> : IEnumerable<T>, ICollection, IReadOnlyCollection<T>, IReadOnlyPersistentColletion<Node<T>>
    {
        private readonly List<Node<T>> heads = new List<Node<T>>();
        private int version;

        private void ThrowIfEmpty()
        {
            if (Count == 0)
                throw new InvalidOperationException($"Свойство {nameof(Count)} было равно {Count}.");
        }

        private static void ThrowIfNull<TObject>(TObject target, string message)
        {
            if (target == null)
                throw new ArgumentNullException(message);
        }

        private void ThrowIfInvalidArrayRange(Array target, int index)
        {
            if (target == null)
                throw new ArgumentNullException(nameof(target));
            if (index < 0 || index >= target.Length)
                throw new IndexOutOfRangeException(nameof(index));
            if (Count > target.Length - index)
                throw new ArgumentOutOfRangeException(nameof(index));
        }

        private void InternalCopyTo(Array target, int index)
        {
            Node<T> current = Head;
            while (current != null)
            {
                target.SetValue(current.Value, index);
                current = current.Next;
                index++;
            }
        }

        /// <summary>
        /// Retrieves the particular version with the specified index.
        /// </summary>
        /// <param name="index">The version index.</param>
        /// <returns>The version.</returns>
        public Node<T> this[int index]
        {
            get
            {
                if (index < 0 || index >= heads.Count)
                    throw new IndexOutOfRangeException(nameof(index));
                return heads[index];
            }
        }

        /// <summary>
        /// The elements count.
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// The synchronization object.
        /// </summary>
        [field: NonSerialized]
        public object SyncRoot { get; } = new object();

        /// <summary>
        /// Determines whether the current collection is a synchronized one.
        /// </summary>
        public bool IsSynchronized => false;

        /// <summary>
        /// The versions count.
        /// </summary>
        public int VersionsCount => heads.Count;

        /// <summary>
        /// The current stack head.
        /// </summary>
        public Node<T> Head => heads[heads.Count - 1];

        /// <summary>
        /// Creates the new instance of PersistentStack from the particular source collection.
        /// </summary>
        /// <param name="sequence">The source collection.</param>
        public PersistentStack(IEnumerable<T> sequence)
        {
            if (sequence == null)
                throw new ArgumentNullException(nameof(sequence));
            heads.Add(null);
            foreach (var item in sequence)
                Push(item);
        }

        /// <summary>
        /// Creates the new empty instance of PersistentStack.
        /// </summary>
        public PersistentStack()
        {
            heads.Add(null);
        }

        /// <summary>
        /// Returns the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        /// <summary>
        /// Returns the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="target">The array which used in copying process.</param>
        /// <param name="index">The index.</param>
        void ICollection.CopyTo(Array target, int index)
        {
            ThrowIfInvalidArrayRange(target, index);
            if (target.Rank != 1)
                throw new ArgumentOutOfRangeException(nameof(target.Rank));
            InternalCopyTo(target, index);
        }

        /// <summary>
        /// Copies the current collection into the particular array from the specified index.
        /// </summary>
        /// <param name="target">The array which used in copying process.</param>
        /// <param name="index">The index.</param>
        public void CopyTo(T[] target, int index)
        {
            ThrowIfInvalidArrayRange(target, index);
            InternalCopyTo(target, index);
        }

        /// <summary>
        /// Returns the current collection enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public Enumerator GetEnumerator()
        {
            return new Enumerator(this);
        }

        /// <summary>
        /// Pushs the particular value into the current stack.
        /// </summary>
        /// <param name="item">The value.</param>
        public void Push(T item)
        {
            int lastIndex = heads.Count - 1;
            heads.Add(heads[lastIndex++]);
            heads[lastIndex] = new Node<T>(item, heads[lastIndex]);
            Count++;
            version++;
        }

        /// <summary>
        /// Removes topmost element from the current stack and returns it.
        /// </summary>
        /// <returns>The topmost element.</returns>
        public T Pop()
        {
            ThrowIfEmpty();
            int lastIndex = heads.Count - 1;
            heads.Add(heads[lastIndex++]);
            T result = heads[lastIndex].Value;
            heads[lastIndex] = heads[lastIndex].Next;
            Count--;
            version++;
            return result;
        }

        /// <summary>
        /// Returns the topmost element of the current stack.
        /// </summary>
        /// <returns>The topmost element.</returns>
        public T Peek()
        {
            ThrowIfEmpty();
            return heads[heads.Count - 1].Value;
        }

        /// <summary>
        /// Clears the current collection.
        /// </summary>
        public void Clear()
        {
            ThrowIfEmpty();
            int lastIndex = heads.Count - 1;
            heads.Add(heads[lastIndex++]);
            heads[lastIndex] = null;
            Count = 0;
            version++;
        }

        /// <summary>
        /// The enumerator.
        /// </summary>
        public struct Enumerator : IEnumerator<T>
        {
            private readonly PersistentStack<T> source;
            private Node<T> current;
            private int index;
            private readonly int version;

            public void ThrowWhenCollectionWasChanged()
            {
                if (version != source.version)
                    throw new InvalidOperationException("Collection was modified");
            }

            /// <summary>
            /// The current item.
            /// </summary>
            object IEnumerator.Current
            {
                get
                {
                    ThrowWhenCollectionWasChanged();
                    return Current;
                }
            }

            /// <summary>
            /// The current item.
            /// </summary>
            public T Current => current.Value;

            /// <summary>
            /// Constructs the new instance of Enumerator from the specified stack.
            /// </summary>
            /// <param name="source">The stack.</param>
            public Enumerator(PersistentStack<T> source) : this()
            {
                ThrowIfNull(source, nameof(source));
                this.source = source;
                index = -1;
                version = source.version;
            }

            /// <summary>
            /// Move the current enumerator on one unit and ensures whether the futher traversion is possible.
            /// </summary>
            /// <returns>true, if the futher traversion is possible, false otherwise.</returns>
            public bool MoveNext()
            {
                ThrowWhenCollectionWasChanged();
                if (index == -1)
                    current = source.Head;
                else
                    current = current.Next;
                return ++index < source.Count;
            }

            /// <summary>
            /// Resets the current enumerator.
            /// </summary>
            public void Reset()
            {
                ThrowWhenCollectionWasChanged();
                index = -1;
            }

            /// <summary>
            /// Releases all unmanaged resources those were used by the current enumerator.
            /// </summary>
            public void Dispose()
            {
            }
        }
    }
}