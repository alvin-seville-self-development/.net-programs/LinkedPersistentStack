namespace PersistentStack
{
    /// <summary>
    /// Represents a data structure with a readonly versions access.
    /// </summary>
    /// <typeparam name="T">Value type.</typeparam>
    public interface IReadOnlyPersistentColletion<T>
    {
        /// <summary>
        /// Retrieves the particular version with the specified index.
        /// </summary>
        /// <param name="index">The version index.</param>
        /// <returns>The version.</returns>
        T this[int index] { get; }

        /// <summary>
        /// The versions count.
        /// </summary>
        int VersionsCount { get; }
    }
}